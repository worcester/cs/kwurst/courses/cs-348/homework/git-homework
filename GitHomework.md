# Homework2 &mdash; Git

## Objective

The objective of this assignment is for you to practice with Git and to
learn more about manipulating branches and remotes.

### Learn Git Branching

Complete "levels" in the [Learn Git Branching](http://pcottle.github.io/learnGitBranching/) tutorial/game.

Notes:

* Feel free to read online documentation about Git if you need more explanation for any of the git commands than is given in a level.
* You may try each "level" as often as you want, you may use `undo` and `reset` commands as often as you want.
* You do not need to match or exceed the number of commands listed for the solution. If you succeed at the level, you can just move on to the next.
* I expect you to do the work on your own - not to use the solution available in *Learn Git Branching* or online solutions to the levels.
* You can use the `levels` command to bring back the *Select a level* dialog, and from there to jump to any particular level. You are not required to complete all of them in order (although some levels may expect the knowledge from a previous level.)
* `git checkout` is the old version of `git switch`. (`git checkout` does more than `git switch`.)


## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

Complete **all** of the following activities in the [Learn Git Branching](http://pcottle.github.io/learnGitBranching/) tutorial:

1. Under the `Main` tab:
    * `Introduction Sequence`
        * 1 - Introduction to Git Commits
        * 2 - Branching in Git
        * 3 - Merging in Git
        * 4 - Rebase Introduction
    * `Ramping Up`
        * 1 - Detach yo' Head
        * 2 - Relative Refs (^)
        * 3 - Relative Refs #2 (~)
        * 4 - Reversing Changes in Git
    * `Moving Work Around`
        * 1 - Cherry-pick Intro
        * 2 -Interactive Rebase Intro
    * `A Mixed Bag`
        * 1 - Grabbing Just 1 Commit
        * 4 - Git Tags
        * 5 - Git Describe
2. Under the `Remote` tab:
    * `Push & Pull -- Git Remotes`
        * 1 - Clone Intro
        * 2 - Remote Branches
        * 3 - Git Fetchin'
        * 4 - Git Pullin'
        * 5 - Faking Teamwork
        * 6 - Git Pushin'

## *Intermediate Add-On*
#### Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher
* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

Complete **any 5** the following activities in the [Learn Git Branching](http://pcottle.github.io/learnGitBranching/) tutorial:

1. Under the `Main` tab:
    * `Advanced Topics`
        * 2 - Multiple Parents
        * 3 - Branch Spaghetti
2. Under the `Remote` tab:
    * `Push & Pull -- Git Remotes`
        * 7 - Diverged History
        * 8 - Locked Main
    * `To Origin And Beyond -- Advanced Git Remotes!`
        * 4 - Git push arguments
        * 5 - Git push arguments -- Expanded
        * 7 - Source of nothing

## *Advanced Add-On*
#### Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher
* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

Complete **any 3** the following activities in the [Learn Git Branching](http://pcottle.github.io/learnGitBranching/) tutorial:

1. Under the `Main` tab:
    * `A Mixed Bag`
        * 2 - Juggling Commits
        * 3 - Juggling Commits #2
    * `Advanced Topics`
        * 1 - Rebasing over 9000 times
2. Under the `Remote` tab:
    * `To Origin And Beyond -- Advanced Git Remotes!`
        * 1 - Push Main! (**Note: Not our recommended workflow!**)
        * 2 - Merging with remotes (**Note: we don't recommend pushing to main**)
        * 3 - Remote Tracking
        * 8 - Pull arguments

#### Deliverables

* You must submit to the Blackboard **Homework2** assignment:

  * A screenshot of the `Main` tab showing green check marks indicating the levels successfully completed.
  * A screenshot of the `Remote` tab showing green check marks indicating the levels successfully completed.
  * Remember, the `levels` command will show you the levels and your completion status.
  * Upload both screenshot files in a single submission.
  * You may resubmit the assignment as many times as you like, but ***only your last submission will be graded.***

&copy; 2023 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
